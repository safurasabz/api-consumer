import httpx
import time
import threading
import logging

class Client:
    def __init__(self):
        self.CLUSTER_NODES = [
            'node1.example.com',
            'node2.example.com',
            'node3.example.com',
        ]

        self.API_BASE_URL = 'http://api.cluster.com/v1'
        self.GROUP_ENDPOINT = '/group/'

        #Logger config
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)


    #Exponential backoff method applied to retry failed requests
    def send_request(self, node, endpoint, json_data=None, method='GET', max_retries=3):
        url = f"{self.API_BASE_URL}{endpoint}"
        retries = 1
        while retries < max_retries:
            try:
                response = httpx.request(method, url, json=json_data, timeout=5) 
                response.raise_for_status()
                return response.json() if response.status_code != 204 else None
            except httpx.TimeoutException:
                self.logger.error(f"Timeout connecting to {node}")
                raise httpx.TimeoutException
            except httpx.HTTPStatusError as e:
                self.logger.error(f"HTTP error {e.response.status_code} connecting to {node}: {e}")
                raise httpx.HTTPStatusError
            except httpx.HTTPError as e:
                self.logger.error(f"Error connecting to {node}: {e}")
                raise httpx.HTTPError
            retries += 1
            self.logger.info(f"Retrying request to {url}... (attempt {retries}/{max_retries})")
            time.sleep(2 ** retries)  
        self.logger.error(f"Failed to connect to {url} after {max_retries} attempts")
        return None

    def create_group(self, group_id):
        actions = {}
        threads = []
        for node in self.CLUSTER_NODES:
            thread = threading.Thread(target=self._create_group_on_node, args=(node, group_id, actions))
            threads.append(thread)
            thread.start()

        for node, action_data in actions.items():
            if action_data['status'] == 'fail':
                self.rollback(actions)
                break

    def _create_group_on_node(self, node, group_id, actions):
        try:
            response = self.send_request(node, f"{self.GROUP_ENDPOINT}", method='POST', json_data={'groupId': group_id})
            if response:
                self.logger.info(f"Group '{group_id}' created successfully on {node}")
                actions[node] = {'group_id': group_id, 'action': 'create', 'status' : 'success'}
            else:
                self.logger.error(f"Failed to create group '{group_id}' on {node}")
                actions[node] = {'group_id': group_id, 'action': 'create', 'status' : 'fail'}
        except Exception as e:
            self.logger.error(f"Error creating group '{group_id}' on {node}: {e}")
            actions[node] = {'group_id': group_id, 'action': 'create', 'status' : 'fail'}

    def delete_group(self, group_id):
        #actions dict stores node, groupID and actions in order to be able to roll back in failure cases
        actions = {}
        threads = []
        for node in self.CLUSTER_NODES:
            thread = threading.Thread(target=self._delete_group_on_node, args=(node, group_id, actions))
            threads.append(thread)
            thread.start()

        for thread in threads:
            thread.join()

        for node, action_data in actions.items():
            if action_data['status'] == 'fail':
                self.rollback(actions)
                break

    def _delete_group_on_node(self, node, group_id, actions):
        try:
            response = self.send_request(node, f"{self.GROUP_ENDPOINT}", method='DELETE', json_data={'groupId': group_id})
            if response:
                self.logger.info(f"Group '{group_id}' deleted successfully from {node}")
                actions[node] = {'group_id': group_id, 'action': 'delete', 'action' : 'success'}
            else:
                self.logger.error(f"Failed to delete group '{group_id}' from {node}")
                actions[node] = {'group_id': group_id, 'action': 'create', 'status' : 'fail'}
        except Exception as e:
            self.logger.error(f"Error deleting group '{group_id}' from {node}: {e}")
            actions[node] = {'group_id': group_id, 'action': 'create', 'status' : 'fail'}


    def rollback(self, actions):
        self.logger.info("Rolling back changes...")
        for node, action_data in actions.items():
            action = action_data['action']
            group_id = action_data['group_id']
            if action == 'create':
                self.send_request(node, f"{self.GROUP_ENDPOINT}", method='DELETE', json_data={'groupId': group_id})
                self.logger.info(f"Rolled back creation of group '{group_id}' on {node}")
            elif action == 'delete':
                self.send_request(node, f"{self.GROUP_ENDPOINT}", method='POST', json_data={'groupId': group_id})
                self.logger.info(f"Rolled back deletion of group '{group_id}' from {node}")