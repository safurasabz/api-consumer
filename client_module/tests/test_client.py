import unittest
from client_module.client import Client

class TestClientModule(unittest.TestCase):

    def test_create_group_on_all_nodes(self):
        group_id = 'test_group'
        self.assertTrue(create_group_on_all_nodes(group_id))

    def test_delete_group_from_all_nodes(self):
        group_id = 'test_group'
        self.assertTrue(delete_group_from_all_nodes(group_id))

if __name__ == '__main__':
    unittest.main()
