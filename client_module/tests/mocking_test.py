import unittest
from unittest.mock import patch, MagicMock
from client import Client

class TestClient(unittest.TestCase):

    @patch('client.httpx')
    def test_send_request(self, mock_httpx):
        client = Client()
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {'key': 'value'}
        mock_httpx.request.return_value = mock_response

        result = client.send_request('node1', '/test', method='GET')

        self.assertEqual(result, {'key': 'value'})
        mock_httpx.request.assert_called_once_with('GET', 'http://api.cluster.com/v1/test', json=None, timeout=5)
        mock_response.raise_for_status.assert_called_once()

    @patch.object(Client, 'send_request')
    def test_create_group(self, mock_send_request):
        client = Client()
        mock_send_request.return_value = {'status': 'success'}
        
        client.create_group('test_group')

        self.assertEqual(mock_send_request.call_count, len(client.CLUSTER_NODES))
        mock_send_request.assert_called_with('node1.example.com', '/group/', method='POST', json_data={'groupId': 'test_group'})

    @patch.object(Client, 'send_request')
    def test_delete_group(self, mock_send_request):
        client = Client()
        mock_send_request.return_value = {'status': 'success'}

        client.delete_group('test_group')

        self.assertEqual(mock_send_request.call_count, len(client.CLUSTER_NODES))
        mock_send_request.assert_called_with('node1.example.com', '/group/', method='DELETE', json_data={'groupId': 'test_group'})

if __name__ == '__main__':
    unittest.main()
