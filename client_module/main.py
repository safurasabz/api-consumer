from client_module.client import Client
import time

class MainApp:
    def __init__(self):
        self.client = Client()

    def run(self):
        actions = {}
        group_id = 'unique_group'
        if self.client.create_group_on_all_nodes(group_id):
            if self.client.delete_group_from_all_nodes(group_id):
                print("Successfully performed actions")
            else:
                print("Error deleting group")
                self.client.rollback(actions)
        else:
            print("Error creating group")

if __name__ == "__main__":
    app = MainApp()
    app.run()
