# Cluster Client Module

## Overview

This project implements a client module for interacting with a cluster of nodes via a RESTful API. It allows for the creation and deletion of objects (groups) across all nodes in the cluster reliably, handling potential API errors and ensuring data consistency.

## Project Structure

- **client_module/**: Contains the client module code and unit tests.
  - `client.py`: Implementation of the client module.
  - `tests/`: Directory for unit tests.
    - `test_client.py`: Unit tests for the client module.

- **Dockerfile**: Instructions for building the Docker image.

- **manifests/**: Kubernetes manifests for deploying the client module.
  - `deployment.yaml`: Defines the deployment configuration.
  - `service.yaml`: Defines the service configuration.

- **requirements.txt**: Lists the dependencies required for the project.

## Installation

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/safurasabz/api-consumer
   cd api-consumer
   ```

2. Install dependencies:
   ```bash
   pip install -r requirements.txt
   ```

## Usage

### Using the Client Module

Execute the `client.py` script to create and delete objects (groups) in the cluster:

```bash
python client_module/client.py
```

### Running Unit Tests

Run unit tests to validate the functionality of the client module:

```bash
python -m unittest discover -s client_module/tests
```

## Docker Image

Build a Docker image containing the client module using the provided Dockerfile:

```bash
docker build -t cluster-client-module .
```

Run the Docker container:

```bash
docker run -d --name cluster-client cluster-client-module
```

## Kubernetes Deployment

Apply the Kubernetes manifests to deploy the client module to a Kubernetes cluster:

```bash
kubectl apply -f manifests/
```